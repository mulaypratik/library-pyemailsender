'''
Created on Apr 16, 2018

@author: Pratik
'''
import smtplib
import ntpath
from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText
from email.MIMEBase import MIMEBase
from email import encoders


class EmailSender:
    'The methods in this class take care of the operations of configuring and delivering the content-rich mail to your desired email addresses.'

    def __init__(self, FromAddress, ToAddresses, CCAddresses, BCCAddresses, Subject, Body, AttachmentFilePath, MailAccountPassword, SMTPName, SMTPPort):
        self.__fromAddress = FromAddress
        self.__toAddresses = ToAddresses
        self.__ccAddresses = CCAddresses
        self.__bccAddresses = BCCAddresses
        self.__subject = Subject
        self.__body = Body
        self.__attachmentFilePath = AttachmentFilePath
        self.__password = MailAccountPassword
        self.__SMTPName = SMTPName
        self.__SMTPPort = SMTPPort
        
    def sendEmail(self):
        try:
            msg = MIMEMultipart()
            
#             From
            msg['From'] = self.__fromAddress
            
#             To
            if self.__toAddresses != None:
                for toAddress in self.__toAddresses:
                    if toAddress != None:
                        msg['To'] = toAddress
            
#             Cc
            if self.__ccAddresses != None:
                for ccAddress in self.__ccAddresses:
                    if ccAddress != None:
                        msg['Cc'] = ccAddress
            
#             Bcc        
            if self.__bccAddresses != None:
                for bccAddress in self.__bccAddresses:
                    if bccAddress != None:
                        msg['BCc'] = bccAddress
                
#             Subject    
            msg['Subject'] = self.__subject
            
#             Body
            msg.attach(MIMEText(self.__body, 'plain'))
            
#             Attachment
            if self.__attachmentFilePath != None:
                for attachmentFile in self.__attachmentFilePath:
                    if attachmentFile != None:
                        head, tail = ntpath.split(attachmentFile)  # To split Filename from path
                        filename = tail or ntpath.basename(head)
                        
                        attachment = open(attachmentFile, "rb")
                         
                        part = MIMEBase('application', 'octet-stream')
                        part.set_payload((attachment).read())
                        encoders.encode_base64(part)
                        part.add_header('Content-Disposition', "attachment; filename= %s" % filename)
                        
                        msg.attach(part)
            
#             Configure SMTP Server 
            server = smtplib.SMTP(self.__SMTPName, self.__SMTPPort)
            server.starttls()
            server.login(self.__fromAddress, self.__password)
            
#             Send mail
            server.sendmail(self.__fromAddress, [self.__toAddresses, self.__ccAddresses, self.__bccAddresses], msg.as_string())
            
            server.quit()
            
            return None
        except Exception as e:
            print e
            return e

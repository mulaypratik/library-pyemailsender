'''
Created on Apr 16, 2018

@author: Pratik
'''
import os
from EmailSender.EmailSender import EmailSender


class EmailTester:
    emailSender = EmailSender("<From's gmail address>", ["<To's email address>"], ["<CC's email address> or None"], None, "Test Mail from EmailSender", "This is a test mail body.", [(os.path.dirname(os.path.realpath(__file__)) + "\\res\\img\\Leo.jpg"), (os.path.dirname(os.path.realpath(__file__)) + "\\res\\img\\Emma.jpg")], "<Your gmail account password>", "smtp.gmail.com", 587)
    print emailSender.sendEmail()

